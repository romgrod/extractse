# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'extractses/version'

Gem::Specification.new do |spec|
  spec.name          = "extractses"
  spec.version       = Extractses::VERSION
  spec.authors       = ["Roman Rodriguez"]
  spec.email         = ["roman.g.rodriguez@gmail.com"]

  spec.summary       = %q{This gem is to provide an esay way to extract session id from a file. Only for chanllenge resolution. This gem will be removed soon}
  spec.description   = %q{Returns a xml file with session id extracted from a given file}
  spec.homepage      = "http://no.homepage.by.now"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.add_dependency "thor"

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
end
