# Extractses

Here is the resolution for the given challenge.

A Ruby gem was created to make the usage/test easier.
Ruby is an interpreted language and, as every language, you should have it installed to run this Ruby script.

Ruby installation:

##Windows
Download an executable to install ruby with ease. Please go to http://rubyinstaller.org/ and download Ruby installer version 2.2.0
To verify installation you can type ruby -v in a console and see the installed version.

##Ubuntu/MAC
In a terminal install rvm, RVM is a Ruby Version Manager...and some other stuff(please see instructions here http://rvm.io/rvm/install)
To verify ruby installation you can type in a terminal and see the version.
	
	ruby -v
	  
If the version was not installed, you can type after installing RVM 
	
	rvm install 2.2.0

##After installing Ruby, you should be ready to use the gem.

As every gem, you have to install it on your system. Gem installation is pretty easy. Just type the following command in a console/terminal:

	gem install extractses

And the gem will be installed on your system.
Once you have it installed you should be ready to use it.

This gem "creates" a command on your system to use it easily.
You have to provide the file name (and path if you are not on the dir of the input file) and provide an output file name to create the session ids xml file.

Suppose you have a file to extract sessions ids from named as: input.ext
And you want to save sessions in a file called: output.xml

you can type on terminal/console the following command:

	extractses input.ext output.xml

And press enter. The XML file will be created with the expected structure in the same directory you are.
