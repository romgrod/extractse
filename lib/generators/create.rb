require_relative '../extractor'
module Extractses
  class Create < Thor::Group

    include Thor::Actions

    desc "Generates empty files for a Cucumber project"

    argument :file_name, :type => :string, :required => true, :desc => "File path to extract sessions from"
    argument :output_filename, :type => :string, :required => true, :desc => "File name to save session xml"

    def extract_ids
      ext = Extractor.new(file_name, output_filename)
      ext.create_output
    end  

  end
end