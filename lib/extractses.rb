require 'thor'

require_relative 'generators/create'

module Extractses
  class Base < Thor

    desc "extract","Extracts sessions ids from a given file"
    def prepare
      Extractses::Create.start([])
    end
  end
end