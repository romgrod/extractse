class Extractor
	def initialize file_path, output_filename
		@start = Time.now.to_f
		file_exist? file_path
		@file_path = file_path
		@output_filename = output_filename
		self.extract!
		elapsed = Time.now.to_f - @start
		puts %q{Elapsed time %.3f seconds}%elapsed
	end

	# Creates XML file
	def create_output
		File.open("#{@output_filename}","a+"){|f| f.write @output}
	end

	# Evaluates if file exists
	def file_exist? file_path
		raise "File does not exist" unless File.exist? file_path
	end

	# Extracts all ids
	def extract!
		ids = []
		file_lines = File.readlines(@file_path)
		file_size = file_lines.size
		if file_size > 1024
			# Extract by parts
			range_start=range_end=0
			loops = file_size / 1024
			loops.times do |i|
				range_start = range_start * (i+1024)
				range_end = (range_end * i)+1023
				ids << get_ids(file_lines[range_start..range_end])
			end

		else
			ids << get_ids(file_lines)
		end
		@output = output_structure ids.sort.flatten
	end

	# Returns an array of ids
	def get_ids lines_array
		ids = lines_array.map do |line|
			print "."
			line.split("|")[1]
		end.sort
		ids
	end

	def output_structure ids
		puts "
#{ids.size} ids has been extracted.
You can see them in a file named as '#{@output_filename}' "
		out = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>
<sessionManager>
    <targetSessions>"
    ids.each do |id|
			out = out + "
      <session sessionID=\"#{id}\" />"
		end
		out = out + "
	</targetSessions>
</sessionManager>"
		out
	end

end